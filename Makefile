ve:
	test ! -d .ve && /usr/local/bin/virtualenv .ve; \
	. .ve/bin/activate; \
	pip install -Ur requirements.txt

run: ve
	. .ve/bin/activate; \
 	python login_to_ukrnet.py; \
 	make clean

clean:
	test -d .ve && rm -rf .ve